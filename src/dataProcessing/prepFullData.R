rm(list=ls())
require(RSQLite)
dataDirectory <- "/disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf"
## open data base connection
sql.driver <- dbDriver("SQLite")
sql.con <- dbConnect(sql.driver,dbname=paste(dataDirectory,"medD-age6566-const.sl3",sep="/"))

cf <- dbReadTable(sql.con,name="data")

pf <- cf[,c("plan","ded_amt","icl_amt","oopt_amt",
            "coin_ded","coin_pre","coin_gap","coin_cat")]

sql.con <- dbConnect(sql.driver,dbname=paste(dataDirectory,"medD-age6566-payHist.sl3",sep="/"))

df <- dbReadTable(sql.con,name="data")
