clear
set more off
set mem 1g
global date 101212

local data_prep 1
local sampling "65"

******************************
  * TO CREATE WEEKS-LEVEL DATA *
 ******************************
  use /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf/Data/extract/65Sample2, clear

drop if missing(srvc_dt)

gen week = week(srvc_dt)
replace week = 0 if week==.

sort bene_id week yr plan ded_amt icl_amt oopt_amt partd_joinmonth plan_*


tempfile 65claims
preserve

        keep bene_id week yr ptpayamt totalcst
        collapse (rawsum) ptpayamt totalcst, fast by(bene_id week yr)
        sort bene_id yr week
        save `65claims', replace

restore

keep bene_id yr plan ded_amt icl_amt oopt_amt partd_joinmonth plan_*
sort bene_id yr 
by bene_id yr: gen dup=_n>1
drop if dup == 1


  forv w = 0/52 {
    gen week`w' = `w'
  }

reshape long week, i(bene_id yr plan)
drop _j
sort bene_id yr week
merge 1:1 bene_id yr week using `65claims', gen(bene_week_merge)

bysort bene_id yr: gen cum_cost=sum(totalcst)

replace ptpayamt=0 if ptpayamt==.
replace totalcst=0 if totalcst==.
drop if week==0
gen month_joinmonth = ym(yr, partd_joinmonth)
gen date_joinmonth = dofm(month_joinmonth)
gen partd_joinweek = week(date_joinmonth)
drop if week<partd_joinweek

gen initial_week_total = totalcst if week<(partd_joinweek+12)

bysort bene_id: egen avg_initial_week = mean(initial_week_total)

tabstat avg_init if ded_amt>0 & !missing(initial_week_total), by(partd_joinmonth)
