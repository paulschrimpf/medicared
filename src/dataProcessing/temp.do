local path /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf
//drop srvc_dt ptpayamt totalcst bnftphas;
//duplicates drop;
//sort bene_id yr;
#delimit ;


plugin call writeSQLite  bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         plan_coin_ded 
                         plan_coin_pre
                         plan_coin_gap 
                         plan_coin_cat 
                         riskscore_CE
                         nogapcov
   , "`path'/medD-full-const.sl3" "1 3"
                         bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         coin_ded
                         coin_pre
                         coin_gap 
                         coin_cat 
                         riskscore_CE
                         nogapcov
;

plugin call writeSQLite  bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         plan_coin_ded 
                         plan_coin_pre
                         plan_coin_gap 
                         plan_coin_cat 
                         riskscore_CE
                         nogapcov
    if age < 67, "`path'/medD-age6566-const.sl3" "1 3"
                         bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         coin_ded
                         coin_pre
                         coin_gap 
                         coin_cat 
                         riskscore_CE
                         nogapcov
;
