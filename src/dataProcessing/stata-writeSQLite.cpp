// Stata plugin to save data as an SQLite database

#include <sqlite3.h>
#include "stplugin.h"
#include <vector>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstring>
#include <set>

using std::vector;
using std::stringstream;
using std::endl;
using std::string;

#define RC_SUCCESS (ST_retcode) 0
#define RC_IO_ERROR (ST_retcode) 198
#define MISSING_CODE -9999

/**********************************************************************/
// Fix SF_display to work with C++ strings 
void mySF_display(const string &str) {
  // need to copy because SF_display cannot take const char *
  char *mesg = new char[str.length()];
  std::copy(str.begin(),str.end(),mesg);
  SF_display(mesg);
  SF_display("\n");
  delete[] mesg;
}
/**********************************************************************/

/**********************************************************************/
// Fix SF_error to work with C++ strings 
void mySF_error(const string &str) {
  // need to copy because SF_display cannot take const char *
  char *mesg = new char[str.length()];
  std::copy(str.begin(),str.end(),mesg);
  SF_error(mesg);
  SF_display("\n");
  delete[] mesg;  
}
/**********************************************************************/

/**********************************************************************/
// This is (loosely) based on http://www.stata.com/support/faqs/data-management/connect_to_mysql.c
STDLL stata_call(int argc, char *argv[]) {
  if (argc<2) {
    mySF_error("Must provide SQLite filename to write to.\n");    
    return(RC_IO_ERROR);
  }
  if (argc != 2+SF_nvars()) {
    stringstream message;
    message << "WARNING: only " << argc-2 << " variable names passed, but "  <<
      SF_nvars() << " variables." << endl << "  The last %d will be named vXX" << endl;  
    mySF_display(message.str());
  } 
  
   
  stringstream query;
  vector<string> varnames(SF_nvars());
  for(int i=0;i<SF_nvars();i++) {
    if ((i+2)<argc) varnames[i] = argv[i+2];
    else {
      stringstream tmp;
      tmp << "v" << i;
      varnames[i] = tmp.str();
    }   
  }

  std::set<int> svIndices;
  stringstream vi;
  int index;
  vi << argv[1];
  while(vi >> index) svIndices.insert(index);
  
  // Create SQLite connection
  sqlite3 *sql;
  int error = 0;
  char *zErrMsg;
  error = sqlite3_open(argv[0],&sql);
  if (error) {
    stringstream message;
    message << "Failed to open SQLite database " 
            << argv[0] << endl;
    mySF_error(message.str());
    return(RC_IO_ERROR);
  }
  
  // Create table

  query << "create table data(";
  mySF_display(query.str());
  for(int i=0;i<SF_nvars();i++) {
    if (i>0) query << " , ";
    query << varnames[i];
  }
  query << ");";
  mySF_display(query.str());
  if (SQLITE_OK != sqlite3_exec(sql,query.str().c_str(),NULL,0,&zErrMsg)) {
    stringstream message;
    message << "SQL error: failed to create table." << endl 
            << " Query = \""<< query.str() << "\"" << endl
            << " SQL Msg = \""<< zErrMsg << "\"" << endl;
    mySF_error(message.str());    
    return(RC_IO_ERROR);
  }  
  
  // Write data row by row
  stringstream insertQuery;
  insertQuery << "insert into data(";  
  for (size_t i=0;i<varnames.size();i++) {
    if (i>0) insertQuery << ",";
    insertQuery << varnames[i];
  }
  insertQuery << ") values(";
  query.clear();
  query.str(std::string());
  query << insertQuery.str();
  for(int c=1;c<=SF_nvars();c++) {
    if (c<SF_nvars()) query << " ?" << c << ", ";
    else query << " ?" << c <<");";
  }
  sqlite3_stmt *stmt;
  sqlite3_exec(sql, "BEGIN TRANSACTION", NULL, NULL, &zErrMsg);
  sqlite3_prepare_v2(sql, query.str().c_str(), query.str().length(), &stmt, NULL);    
  for(int r=SF_in1();r<=SF_in2(); r++){
    if(SF_ifobs(r)) {
      //mySF_display(query.str());
      for(int c=1;c<=SF_nvars();c++) {
        ST_retcode rc;
        if (svIndices.find(c) != svIndices.end() ) {
          char buff[200];
          if( (rc = SF_sdata(c,r,buff)) ) return(rc) ;
          sqlite3_bind_text(stmt, c, buff, strlen(buff), SQLITE_TRANSIENT);
        } else {
          ST_double val;
          if( (rc = SF_vdata(c,r,&val)) ) return(rc) ;
          if (SF_is_missing(val)) {
            sqlite3_bind_double(stmt, c, MISSING_CODE);
          } else {
            sqlite3_bind_double(stmt, c, val);
          }
        }
      }
      if (SQLITE_DONE != sqlite3_step(stmt)) {
        stringstream message;
        message << "SQL error while writing table." << endl;
        mySF_error(message.str());
        return(RC_IO_ERROR);
      }
      sqlite3_reset(stmt);
    }
  }
  sqlite3_exec(sql, "COMMIT TRANSACTION", NULL, NULL, &zErrMsg);
  sqlite3_finalize(stmt);  
  sqlite3_close(sql);
  sqlite3_free(zErrMsg);
  return(RC_SUCCESS);
}
/**********************************************************************/
