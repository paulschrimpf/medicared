#ifndef COMPUTEVALUEFUNCTION_HPP 
#define COMPUTEVALUEFUNCTION_HPP 
//#include "alglib/interpolation.h"
//#include "alglib/specialfunctions.h"
#include "fastSpline.hpp"

#include "oopFunc.hpp"
#include "omegaDist.hpp"
#include "data.hpp"
#include <vector>
using std::vector;
using dataNS::array;

using fastSpline::spline1dinterpolant;

void computeValueFunction(spline1dinterpolant **vsplines, 
                          const omegaDist<double> *omega,                           
                          const double delta, const double *lambda, 
                          const double *thetaV, const double *thetaP, 
                          const int nTheta,
                          const double *thetaMixP, const int nThetaMix,
                          const int T, const vector< vector<double> > &x, const vector<int> &nx, 
                          const vector<oopFunc> &oopfs, 
                          const int nplan, 
                          const array<double,3> &transProb,
                          const double riskAversion,
                          const bool terminalV,
                          const double deltaTheta, const double deltaOmega,
                          const vector<double> powDelta,
                          const vector<double> powDeltaTheta,
                          const vector<double> powDeltaOmega,
                          const double pOT
                          );
/* evaluates value function: 
   We create this wrapper so that if vf is class valueFunction and 
   x is double, we return vf(x), but if x is F<double>, we also return
   derivates of vf(x) with respect to parameters.
 */
class valueFunction {
public:
  spline1dinterpolant *v;
  int nparam;
  double eval(const double &x, double *type) const;
};


#endif
