#ifndef _OMEGADIST_HPP
#define _OMEGADIST_HPP

#include<vector>
using std::vector;

template<typename T> 
class transforms {
 public:
  //transforms() {}
  virtual T f(const T&) = 0;
  virtual T finv(const T&) = 0 ; 
  virtual T F(const T&) = 0;
  virtual T getRA() = 0;
};

template<typename T> 
class CARAtransforms : public transforms<T> {
private: 
  T riskAversion;
public:
  CARAtransforms();
  CARAtransforms(T r);
  T f(const T &x);
  T finv(const T &x);
  T F(const T &x);
  T getRA();
};

template<typename T>
class IDtransforms : public transforms<T> {
public:
  IDtransforms();
  T f(const T &x);
  T finv(const T &x);
  T F(const T &x);
  T getRA();
};


template<typename T>
class omegaDist //: public omegaDist
/* mixture of uniform (0,1) with probability p and 1 with probability
   1-p */
{
public:
  T p;
  int nparam() const;
  vector<T> param();
  omegaDist(T pin); 
  omegaDist();
  void setparam(T *pin);
  T cdf(T x) const; 
  T pdf(T x) const;

  // E[omega * 1(omega > sign(direction)*cut) ] 
  T truncEx(const T &cut, int direction) const;

  // E[f(omega + v) * 1(omega/theta > sign(direction)*cut)] 
  T truncEx(const T &cut, int direction, 
            transforms<T> *tr, 
            const T &theta, const T &v
            ) const;

  T dtruncEx(T cut, int direction) const;
  vector<T> dcdf_dparam(T x) const;
  vector<T> dtruncEx_dparam(T cut, int direction) const;
};

template<typename T>
class thetaDist 
{ 
public:
  int nparam() const;
  T mu;
  T sig;
  thetaDist();
  thetaDist(double muin, double sigin) ;
  T pdf(T x) const;
};


#endif
