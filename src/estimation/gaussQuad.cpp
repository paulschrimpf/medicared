#include "gaussQuad.hpp"
#include <math.h>
#define _USE_MATH_DEFINES
#include "GaussQR/source/gaussqr.h"

#include <iostream>
#include <vector>
using std::cout;
using std::endl;
using std::vector;

namespace gaussQuad {

double normcdf(double x) {
  return(0.5 + 0.5*erf(x/sqrt(2)));
}

/************************************************************************/
/** normalPDF class */
normalPDF::normalPDF() { 
  mu = 0; 
  sig =1.0;
}
normalPDF::normalPDF(double m, double s) { 
  mu=m; 
  sig=s;
}

double normalPDF::operator()(double x) const {
  double z=(x - mu)/sig;
  return(exp(-0.5*z*z)/(sig*sqrt(2*M_PI)));
}

domain normalPDF::getDomain() const {
  domain dom;
  dom.loFinite = dom.hiFinite=false;
  dom.lo = -1e307;
  dom.hi = 1e307;
  return(dom);
}
/************************************************************************/

/************************************************************************/
/** lognormalPDF class */
lognormalPDF::lognormalPDF() {
  mu = 0.0; 
  sig=1.0;
  truncated = false;
  probNotTruncated = 1.0;
}

lognormalPDF::lognormalPDF(double m, double s) {
  mu = m;
  sig = s;
  truncated = false;
  probNotTruncated = 1.0;
}

lognormalPDF::lognormalPDF(double m, double s, double hi) {
  mu = m;
  sig = s;
  truncated = true;
  max = hi;
  probNotTruncated = normcdf( (log(max) - mu)/sig );
}

double lognormalPDF::operator()(double x) const {
  if (truncated) {
    if (x > max) return(0);
    double z = (log(x) - mu)/sig;
    return(1/(sig*x*sqrt(2*M_PI))*exp(-0.5*z*z)/probNotTruncated);
  } else {
    double z = (log(x) - mu)/sig;
    return(1/(sig*x*sqrt(2*M_PI))*exp(-0.5*z*z) );
  }
}

domain lognormalPDF::getDomain() const {
  domain dom;
  dom.loFinite=true;
  dom.lo = 0.0;
  if (truncated) {
    dom.hiFinite=true;
    dom.hi = max;
  } else { 
    dom.hiFinite = false;
    dom.hi = 1e307;
  }
  return(dom);
}

/************************************************************************/
int gaussianQuadrature(PDF *pdf,  unsigned int nint, 
                       vector<double> &xint, vector<double> &wint) {
  const integer_t n = 1023;
  real_t z[n], q[n], x[n], dx[n],w[n], a0[n], b0[n];
  int gqr;
  domain_type dt;
  double left, right;
  domain dom = pdf->getDomain();
  if (dom.loFinite && dom.hiFinite) dt = domain_finite;
  else if (dom.loFinite && !dom.hiFinite) dt = domain_right_infinite;
  else if (!dom.loFinite && dom.hiFinite) dt = domain_left_infinite;
  else dt = domain_infinite;
  left = dom.lo;
  right = dom.hi;

  gqr = fejer2_abscissae(n,z,q);
  if ( gqr != gaussqr_success ) {
    cout << "ERROR gaussianQuadrature : fejer2_abscissae returned " << gqr << endl;
    exit(gqr);
  }
  
  gqr = map_fejer2_domain(left,right,dt,n,z,x,dx);
  if ( gqr != gaussqr_success ) {
    cout << "ERROR gaussianQuadrature : map_fejer2_domain returned " << gqr << endl;
    exit(gqr);
  }

  for ( integer_t i = 0; i < n; i++ ) {
    w[i] = dx[i]*q[i]*(*pdf)(x[i]);
  }

  // calculate the recursion coefficients a and b from the quadrature scheme
  gqr = lanczos_tridiagonalize(n,x,w,a0,b0);
  if ( gqr != gaussqr_success ) {
    cout << "ERROR gaussianQuadrature : lanczos_tridiagonalization returned " << gqr << endl;
    exit(gqr);
  }
  
  if (xint.size()!=nint) xint = vector<double>(nint);
  if (wint.size()!=nint) wint = vector<double>(nint);
  // calculate abscissa and weights
  gqr = gaussqr_from_rcoeffs(nint,a0,b0,&xint[0],&wint[0]);
  if ( gqr != gaussqr_success ) {
    cout << "ERROR gaussianQuadrature : gaussqr_from_rcoeffs returned " << gqr << endl;
    exit(gqr);
  }

  return(0);
}

} // end namespace gaussQuad
