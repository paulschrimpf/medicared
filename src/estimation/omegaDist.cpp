#include "omegaDist.hpp"

#define dx 1e-6
#include <vector>
#include <limits>
#include <iostream>
#include "alglib/specialfunctions.h"

using alglib::beta;
using alglib::incompletebeta;
using std::vector;
using std::numeric_limits;
using std::cout;
using std::endl;

/**********************************************************************/
template<typename T> 
CARAtransforms<T>::CARAtransforms() {}

template<typename T> 
CARAtransforms<T>::CARAtransforms(T r) { riskAversion = r; }

template<typename T> 
T CARAtransforms<T>::f(const T &x) {
  return(exp(-riskAversion*x));
}

template<typename T> 
T CARAtransforms<T>::finv(const T &x) {
  return(-1/riskAversion*log(x));
}
template<typename T> 
T CARAtransforms<T>::F(const T &x) {
  return(-exp(-riskAversion*x)/riskAversion);
}

template<typename T> 
T CARAtransforms<T>::getRA() {
  return(riskAversion);
}

////

template<typename T>
IDtransforms<T>::IDtransforms() { }

template<typename T>
T IDtransforms<T>::f(const T &x) {return(x);}

template<typename T>
T IDtransforms<T>::finv(const T &x) {return(x);}

template<typename T>
T IDtransforms<T>::F(const T &x) {return(x*x*0.5);}

template<typename T>
T IDtransforms<T>::getRA() {return(0);}



// instantiated needed templates
template class CARAtransforms<double>;
template class IDtransforms<double>;

/**********************************************************************/
template<typename T> 
omegaDist<T>::omegaDist(T pin) { p = pin; }

template<typename T>
omegaDist<T>::omegaDist() { p = 0; }

template<typename T>
void omegaDist<T>::setparam(T *pin) { p = pin[0]; }

template<typename T>
int omegaDist<T>::nparam() const { return 1; }

template<typename T>
vector<T> omegaDist<T>::param() {
  vector<T> x(1);
  x[0] = p;
  return(x);
}
  
template<typename T>
T omegaDist<T>::cdf(T x) const
{
  if (x<=0) return(0);
  else if (x<1.0) return(p*x);
  else return(1.0);
  /*
  const T nearOne = 0.9;
  if (x<=0) return(0);
  else if (x<nearOne) return(p*x);
  else if (x<1) {
    // smooth function from nearOne to 1 with 
    // f(nearOne) = p*nearOne
    // f'(nearOne) = p
    // f(1) = 1
    // f'(1) = 0
    T y = (x-nearOne)/(1.0-nearOne);
    T f = p*x + y*y*((3-(2+nearOne)*p) + (p*(1+nearOne)-2)*y);
    //printf("x = %f, p=%f, cdf=%f\n",x,p,f);
    return(f);
  } else return(1.0);
  */
}

template<typename T>
T omegaDist<T>::pdf(T x) const
{
  if (x<=0) return(0);
  else if (x>1) return(1);    
  else return(p);
}


// E[omega * 1(omega > sign(direction)*cut) ] , 
// where omega is U(0,1) with prob p and 1 with prob (1-p)
template<typename T>
T omegaDist<T>::truncEx(const T &cut, int direction) const {
  if (direction<=0) {
    if (cut<=0) return(0);
    else if (cut>=1) return(0.5*p + (1-p));
    else return(p*cut*cut*0.5);
  } else {
    if (cut<=0) return(0.5*p+(1-p));
    else if (cut>=1) return(0);
    else return(1-p + p*(1-cut*cut)*0.5);
  }
}


// E[f(-omega + v) * 1(omega/theta > sign(direction)*cut) ], 
// where omega/theta is U(0,1) with prob p and 1 with prob (1-p)
template<typename T>
T omegaDist<T>::truncEx(const T &cut, int direction, 
                        transforms<T> *tr, 
                        const T &theta, const T &v
                        ) const {
  if (direction<=0) {
    if (cut<=0) return(0);
    else if (cut>=1)
      return( (tr->F(-theta + v) - tr->F(v))*p/(-theta) + (1-p)*tr->f(-theta+v));
    else return(p* (tr->F(-theta*cut + v) - tr->F(v))/(-theta));
  } else {
    if (cut<=0) return(p*(tr->F(-theta+v) - tr->F(v))/(-theta) + (1-p)*tr->f(-theta+v));
    else if (cut>=1) return(0);
    else return((1-p)*tr->f(-theta+v) + 
                p*(tr->F(-theta*cut + v) - tr->F(v))/(-theta));
  }
}

template<typename T>
T omegaDist<T>::dtruncEx(T cut, int direction) const {
  cout << "ERROR: dtruncEx no longer up to date" << endl;
  exit(1);
  if (cut<=0 || cut>=1) return(0);
  else return(p*0.5);
}

template<typename T>
vector<T> omegaDist<T>::dcdf_dparam(T x) const {
  vector<T> dcdf(1);
  if (x<0 || x>(1+100*numeric_limits<double>::epsilon() ))
    dcdf[0] = 0;
  else dcdf[0] = x;
  return(dcdf);
}

template<typename T>
vector<T> omegaDist<T>::dtruncEx_dparam(T cut, int direction) const {
  cout << "ERROR: dtruncEx no longer up to date" << endl;
  exit(1);
  vector<T> dtex(1);
  if (direction<=0) {
    if (cut<=0) dtex[0] = 0;
    else if (cut>=1) dtex[0] = -0.5;
    else dtex[0] = cut*0.5;
  } else {
    if (cut<=0) dtex[0] = -0.5;
    else if (cut>=1) dtex[0] = 0;
    else dtex[0] = -1+(1+cut)*.5;
  }
  return(dtex);
}


/**********************************************************************/
// instantiated needed templates
template class omegaDist<double>;


/**********************************************************************/
// thetaDist 

template<typename T>
int thetaDist<T>::nparam() const { return (2); }

template<typename T>
thetaDist<T>::thetaDist() {}

template<typename T>
thetaDist<T>::thetaDist(double muin, double sigin) 
{
  mu = muin; 
  sig = sigin;
}

template<typename T>
T thetaDist<T>::pdf(T x) const {
  T f = (log(x) - mu)/sig;
  f *= -f/2;
  f = exp(f)/(x*sig*sqrt(2*M_PI));
  return(f);
}

template class thetaDist<double>;
