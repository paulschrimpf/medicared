#include "fastSpline.hpp"
#include <vector>
using std::vector;

#include "alglib/interpolation.h"
using alglib::real_1d_array;

namespace fastSpline {
spline1dinterpolant::spline1dinterpolant() : alglib::spline1dinterpolant() {}

spline1dinterpolant::spline1dinterpolant(const spline1dinterpolant &rhs) : alglib::spline1dinterpolant(rhs) 
{
  node = rhs.node;
  xcut = rhs.xcut;
  delx = rhs.delx;
}


void buildspline(const real_1d_array &x, const real_1d_array &y, spline1dinterpolant &c) {
  //alglib::spline1dbuildmonotone(x,y, c);
  alglib::spline1dbuildlinear(x,y, c);
  c.node.clear();
  c.xcut.clear();
  c.delx.clear();
  c.node.push_back(0);
  c.xcut.push_back(x[0]);
  c.delx.push_back(x[1]-x[0]);
  for(int i=1;i<(x.length()-1);i++) {
    double dx = x[i+1]  - x[i];
    if ((dx - c.delx.back()) > 1e-8 || (dx - c.delx.back()) < -1e-8) { 
      c.node.push_back(i);
      c.xcut.push_back(x[i]);
      c.delx.push_back(dx);
    }
  }
}
  
double spline1dcalc(const spline1dinterpolant &spline, const double x) 
{
  //return(alglib::spline1dcalc(spline,x));
  // locate x among nodes
  int l = 0;
  int m = 0;
  int r = (int) spline.xcut.size();
  alglib_impl::spline1dinterpolant *c = spline.c_ptr();
  while (l!=r-1) {
     m = (l+r)/2;
    if( spline.xcut[m]>=x ) r = m; 
    else  l = m;
  }
  int xindex = spline.node[l] + ((int) ((x - spline.xcut[l])/spline.delx[l]));
  if (xindex > (c->x.cnt-2) || 
      xindex<0) // this is possible when (int) overflows 
    xindex = c->x.cnt-2;
  // Interpolation
  double z = x-c->x.ptr.p_double[xindex];
#ifndef NDEBUG
  l = 0;
  r = c->n-2+1;
  while(l!=r-1)
    {
      m = (l+r)/2;
      if( c->x.ptr.p_double[m]>=x )
        {
          r = m;
        }
      else
        {
          l = m;
        }
    }
  if (z!=0 && l!=xindex) {
    printf("Error: l!=xindex %d %d\n",l,xindex);
  }
  if (z<0) { 
    printf("Error: x < x.ptr.p_double[xindex]\n"); 
  }
  if ((xindex + 2) < c->x.cnt && x>c->x.ptr.p_double[xindex+1]) {
    printf("Error: x > x.ptr.p_double[xindex+1]\n");
  }
#endif

  /*
  if (xindex>=c->x.cnt || 
      ((xindex<c->x.cnt-1) && !(x>= c->x.ptr.p_double[xindex] && x<=c->x.ptr.p_double[xindex+1]))) {
    printf("bad\n");
  }*/
  m = 4*xindex;
  double result = c->c.ptr.p_double[m] + 
    z*(c->c.ptr.p_double[m+1]  
       );//     +z*(c->c.ptr.p_double[m+2]+z*c->c.ptr.p_double[m+3])); 
  // not needed for linear splines, add back in for cubic  
  return result; 
}

} // end namespace fastSpline
