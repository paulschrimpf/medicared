// Wrapper to run Rcpp code inside gdb. Based on:
// http://stackoverflow.com/questions/4980595/how-to-debug-an-r-package-with-c-code-in-emacs-using-gdb

#include <Rcpp.h>
#include <RInside.h>
#include "cf.hpp"

int main(int argc, char *argv[]) 
{
  using namespace std;
  using namespace Rcpp;

  RInside R(argc, argv);

  string evalstr = "args <- c(\"../c/data/p20.txt\",\"../c/x0.txt\");\n"
    "names(args) <- c(\"datafile\",\"xfile\")";
  R.parseEvalQ(evalstr);
  
  SEXP args = R["args"];
  
  R["result"] = cfsim(args);
  
  evalstr = "print(summary(result))";
  R.parseEvalQ(evalstr);

  return 0;
}
