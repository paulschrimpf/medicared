#include <omp.h>
#include <vector>
#include <algorithm>

#include "Rcpp.h"

using std::vector;
using namespace Rcpp;


// Epanechnikov kernel
inline double kern(const double x, const double xi, const double bw) {
  double y = (x-xi)/bw;
  y *= y;
  return(y<1? 0.75*(1.0-y):0.0);
}

// kernel regression
// [[Rcpp::export]] 
NumericVector kernreg(NumericVector xeval, NumericVector xdat, NumericVector ydat,
                      double bw, NumericVector weight) {
  NumericVector yeval(xeval.size());
  if (xdat.size() != ydat.size() || xdat.size() != weight.size()) {
    Rcpp::Rcout <<"Errror bad input sizes " << xdat.size() <<  " " 
                << ydat.size() << " " << weight.size() << std::endl;
    return(yeval);
  }
  if (bw<=0) {
    // use silverman's rule of thumb
    double mu = 0, var=0, sumW=0;
    for(int i=0;i<xdat.size();i++) {
      mu+=xdat[i];
      sumW += weight[i];
    }
    mu /= sumW;
    for(int i=0;i<xdat.size();i++) var+=(xdat[i]-mu)*(xdat[i]-mu)*weight[i];
    var/= sumW;
    bw = sqrt(var)*pow(xdat.size(),-.2);
  }
  //#pragma omp parallel for default(shared) 
  for(int j=0;j<xeval.size();j++) {
    double num = 0, den = 0;
    for(int i=0;i<xdat.size();i++) {
      double kx = kern(xeval[j],xdat[i],bw)*weight[i];
      num += kx*ydat[i];
      den += kx;
    }
    yeval[j] = num/den;
  }
  return(yeval);
}


class sortVector {
public:
  sortVector(NumericVector *_y) { y = _y; }
  NumericVector *y;
  bool operator()(const int &i,const int &j) const 
  { return((*y)[i]<(*y)[j]); }
};

// quantile kernel regression

// [[Rcpp::export]]
NumericMatrix kernquantreg(NumericVector xeval, NumericVector quantiles, 
                           NumericVector xdat, NumericVector ydat, double bw,
                           NumericVector weight) {
  NumericMatrix yeval(xeval.size(),quantiles.size());
  if (xdat.size() != ydat.size() || xdat.size() != weight.size()) {
    Rcpp::Rcout <<"Errror bad input sizes " << xdat.size() <<  " " 
                << ydat.size() << " " << weight.size() << std::endl;
    return(yeval);
  }
  if (bw<=0) {
    // use silverman's rule of thumb
    double mu = 0, var=0, sumW = 0;    
    for(int i=0;i<xdat.size();i++) {
      mu+=xdat[i];
      sumW += weight[i];
    }
    mu /= sumW;
    for(int i=0;i<xdat.size();i++) var+=(xdat[i]-mu)*(xdat[i]-mu)*weight[i];
    var/= sumW;
    bw = sqrt(var)*pow(xdat.size(),-.2);
  }
  vector<int> si(ydat.size());
  for(size_t i=0;i<si.size();i++) si[i] = i;
  std::sort(si.begin(),si.end(), sortVector(&ydat));
  std::sort(quantiles.begin(),quantiles.end());
  //#pragma omp parallel for default(shared) 
  for(int j=0;j<xeval.size();j++) {
    double den = 0;
    vector<double> w(xdat.size());
    for(int i=0;i<xdat.size();i++) {
      den += (w[i] = kern(xeval[j],xdat[i],bw)*weight[i]);
    } 
    int i =0;
    double p = 0;
    for (int q=0;q<quantiles.size();q++) {
      while(p<(quantiles[q]*den) && i<ydat.size()) p+=(w[si[i++]]);
      if (i==0) yeval(j,q) = ydat[si[0]];
      else yeval(j,q) = 0.5*(ydat[si[i]]+ydat[si[i-1]]);
    }
  }
  return(yeval);
}


// [[Rcpp::export]]
NumericVector lagPanel(NumericVector x, vector<int> id, vector<int> time) {
  NumericVector xlag(x.size(),NA_REAL);
  for(int j=1;j<x.size();j++) {
    if (id[j]==id[j-1] && time[j]-1==time[j-1]) xlag[j] = x[j-1];      
  }
  return(xlag);
}

